import logging
from logging import info
from requests.api import delete
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename='dev.log')
info('Loading dependencies...')
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import Updater, CallbackContext, CommandHandler, MessageHandler, ConversationHandler, Filters, CallbackQueryHandler
from api import KEYS_API, APIException



class ST:
    MAIN_KEYBOARD_LIST = [[InlineKeyboardButton("ключи", callback_data='ключи')], [InlineKeyboardButton("учителя", callback_data='учителя')]]
    MAIN_KEYBOARD_ADD_TEACHER = 'Добавить пользователя'
    allowed_uid = ['170575465', '600128351', '1096174342']
    CANCEL_KEYBOARD_INLINE = [InlineKeyboardButton("закрыть", callback_data='delmsg')]
    ASKNAME, ASKPOSITION, CONFIRM, FINISH = range(4)
    CONFIRM_INLINE_KEYBOARD = [
        [
            InlineKeyboardButton("ДА", callback_data='chat_yes'),
            InlineKeyboardButton("НЕТ", callback_data='chat_no'),
        ]
    ]
    CANCEL_INLINE_KEYBOARD = [
        [
            InlineKeyboardButton("ОТМЕНИТЬ ВСЕ", callback_data='chatcancel'),
        ]
    ]



class bot:
    chat = {}   
    def __init__(self):
        self.bot = Updater('1470615684:AAG1A6VVryqBgRdPnre4rCZLjl16BoVc6Jw')
        self.dispatcher = self.bot.dispatcher
        self.dispatcher.add_handler(CallbackQueryHandler(self.list_keys, pattern=r'ключи'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.list_ticher, pattern=r'учителя'))
        self.dispatcher.add_handler(CommandHandler('start',self.start))
        self.dispatcher.add_handler(CommandHandler('help',self.help))
        self.chat_handler = ConversationHandler(
            entry_points=[CommandHandler('newteacher', self.chat_init)],
            states={
                ST.ASKNAME: [MessageHandler(Filters.all, self.chat_askname)],
                ST.ASKPOSITION: [MessageHandler(Filters.all, self.chat_askposition)],
                ST.CONFIRM: [MessageHandler(Filters.all, self.chat_askposition)],
            },
            fallbacks=[CallbackQueryHandler(self.chat_cancel, pattern=r'^chatcancel$')]
        )
        self.chat_handler2 = ConversationHandler(
            entry_points=[CommandHandler('newkey', self.chat_init2)],
            states={
                ST.ASKPOSITION: [MessageHandler(Filters.all, self.chat_askposition2)],
                ST.CONFIRM: [MessageHandler(Filters.all, self.chat_askposition2)],
            },
            fallbacks=[CallbackQueryHandler(self.chat_cancel, pattern=r'^chatcancel$')]
        )
        self.chat_handler_rename = ConversationHandler(
            entry_points=[CallbackQueryHandler(self.init_rename_teacher, pattern=r'^rename teacher \d*$')],
            states={
                ST.ASKPOSITION: [MessageHandler(Filters.all, self.ask_name)],
                ST.CONFIRM: [MessageHandler(Filters.all, self.chat_askposition3)],
            },
            fallbacks=[CallbackQueryHandler(self.chat_cancel, pattern=r'^chatcancel$')]
        )
        self.dispatcher.add_handler(self.chat_handler)
        self.dispatcher.add_handler(self.chat_handler2)
        self.dispatcher.add_handler(self.chat_handler_rename)
        self.dispatcher.add_handler(CallbackQueryHandler(self.start, pattern=r'^startwq$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.user_info, pattern=r'^get_user_info \d* \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.key_info, pattern=r'^get_key_info \d* \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.delete_message, pattern=r'^delmsg$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.delete_ticher, pattern=r'^deltich \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.delete_key, pattern=r'^delkey \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.list_ticher,pattern=r'^list_ticher \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.list_keys,pattern=r'^list_keys \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.card_list,pattern=r'^card_list \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.mycards, pattern=r'^cardqw \d* \d*$'))
        self.dispatcher.add_handler(CallbackQueryHandler(self.rmcard, pattern=r'^rmcard \d* \d*$'))
        self.dispatcher.add_handler(CommandHandler('help',self.start))

    def debug(self, update:Update, context:CallbackContext):
        print(update.callback_query.data)
        update.callback_query.message.reply_text(update.callback_query.data)

    def start(self, update:Update, context:CallbackContext):
        key = []
        key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'delmsg')])
        if update.callback_query:
            uid = update.callback_query.message.chat.id
            for i in ST.allowed_uid:
                if i == str(uid):
                    print(uid)
                    update.callback_query.message.edit_text('меню') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
                    update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(ST.MAIN_KEYBOARD_LIST,one_time_keyboard=False, resize_keyboard=True))
                    break
            else:
                update.callback_query.message.edit_text('доступ запрещен')
        else:
            uid = update.message.chat.id
            update.message.delete()
            print(uid)
            #update.message.reply_text('hi i am robot',reply_markup=InlineKeyboardMarkup(key,one_time_keyboard=False, resize_keyboard=True))
            for i in ST.allowed_uid:
                if i == str(uid):
                    update.message.reply_text('доступ разрешен',reply_markup=InlineKeyboardMarkup(ST.MAIN_KEYBOARD_LIST, one_time_keyboard=False, resize_keyboard=True, per_message=True))
                    break
            else:
                update.message.reply_text(f'доступ запрещен\nваш Telegram user id: {uid}\nОбратитесь к @Shyam134', reply_markup=InlineKeyboardMarkup(key, one_time_keyboard=False, resize_keyboard=True))

    def help(self, update:Update, context:CallbackContext):
        update.message.delete()
        key = []
        key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'delmsg')])
        update.message.reply_text('для меню нажмите /start \nдля добавления пользователя /newteacher\nдля добавления карточки /newcard',reply_markup=InlineKeyboardMarkup(key, one_time_keyboard=False, resize_keyboard=True))

    def chat_init(self, update:Update, context:CallbackContext):
        update.message.delete()
        self.chat[update.message.from_user.id] = {}
        update.message.reply_text(
            'Ок, новый сотрудник! Как его зовут? (ФИО)',
            reply_markup=InlineKeyboardMarkup(ST.CANCEL_INLINE_KEYBOARD, one_time_keyboard=False)
        )
        return ST.ASKNAME

    def chat_askname(self, update:Update, context:CallbackContext):
        employee = update.message.text
        self.chat[update.message.from_user.id]['name'] = employee
        update.message.reply_text(
            f'Принял! На какой должности работает {employee}?',
        )
        return ST.ASKPOSITION

    def chat_askposition(self, update:Update, context:CallbackContext):
        update.message.delete()
        employee = self.chat[update.message.from_user.id]['name']
        position = update.message.text
        self.chat[update.message.from_user.id]['position'] = position
        update.message.reply_text(
            f'Ну все, записали!')
        KEYS_API.addTeacher(self.chat[update.message.from_user.id]['name'], self.chat[update.message.from_user.id]['position'])
        return ConversationHandler.END

    def chat_init2(self, update:Update, context:CallbackContext):
        update.message.delete()
        self.chat[update.message.from_user.id] = {}
        update.message.reply_text(
            'Ок, новый ключ! откуда он?',
            reply_markup=InlineKeyboardMarkup(ST.CANCEL_INLINE_KEYBOARD, one_time_keyboard=False)
        )
        return ST.ASKPOSITION

    def init_rename_teacher(self, update:Update, context:CallbackContext):
        _, _, pk = update.callback_query.data.split()
        self.chat[update.callback_query.message.chat.id] = {}
        self.chat[update.callback_query.message.chat.id]['pk'] = pk
        update.callback_query.message.edit_text(
            'как по новому будут звать учителя?'
        )
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(ST.CANCEL_INLINE_KEYBOARD,one_time_keyboard=False, resize_keyboard=True))
        return ST.ASKPOSITION

    def ask_name(self, update:Update, context:CallbackContext):
        position = update.message.text
        print(1)
        self.chat[update.message.from_user.id]['name'] = position
        update.message.reply_text(
            f'{position} такс окей, где он работает?')
        return ST.CONFIRM

    def chat_askposition3(self, update:Update, context:CallbackContext):
        position = update.message.text
        self.chat[update.message.from_user.id]['position'] = position
        KEYS_API.changeTeacher(self.chat[update.message.from_user.id]['pk'], self.chat[update.message.from_user.id]['name'], self.chat[update.message.from_user.id]['position'])
        update.message.reply_text(
            f'успешно')
        return ConversationHandler.END

    def chat_askposition2(self, update:Update, context:CallbackContext):
        position = update.message.text
        self.chat[update.message.from_user.id]['position'] = position
        update.message.reply_text(
            f'{position} такс окей, где он работает?')
        KEYS_API.addKey(self.chat[update.message.from_user.id]['key'])
        return ConversationHandler.END

    def chat_cancel(self, update:Update, context:CallbackContext):
        update.callback_query.message.reply_text(
            'Ок. Забыли. Это останется между нами...'
        )
        return ConversationHandler.END

    def chat_finish(self, update:Update, context:CallbackContext):
        update.message.reply_text(
            'Угу...',
        )
        return ConversationHandler.END

    def list_ticher(self, update:Update, context:CallbackContext):
        #if update.callback_query:
        #    uid = update.callback_query.message.chat.id
        #else:
        #    uid = update.message.chat.id
        try:
            list_bottuns = KEYS_API.getTeachers()
            if list_bottuns:
                page = 0
                if update.callback_query:
                    try:
                        _, readpage = update.callback_query.data.split()
                        page = int(readpage)
                    except Exception:
                        pass
                object_in_page = 5
                ticher = []
                for i in list_bottuns[object_in_page*page:object_in_page*page + object_in_page]:
                    name = i['name']
                    pk = i['pk']
                    ticher.append([InlineKeyboardButton(name, callback_data=f'get_user_info {pk} {page}')])
                prevnext = []
                if page > 0:
                    prevnext.append(InlineKeyboardButton(f'{page} <<', callback_data=f'list_ticher {page-1}'))
                if len(list_bottuns) > object_in_page * (page + 1):
                    prevnext.append(InlineKeyboardButton(f'>> {page+2}', callback_data=f'list_ticher {page+1}'))
                if prevnext:
                    ticher.append(prevnext)
                ticher.append([InlineKeyboardButton(f'Закрыть', callback_data=f'startwq')])
                if update.callback_query:
                    update.callback_query.message.edit_text('все учителя:') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
                    update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(ticher,one_time_keyboard=False, resize_keyboard=True))
                else:
                    update.message.reply_text("все учителя:", reply_markup=InlineKeyboardMarkup(ticher,one_time_keyboard=False, resize_keyboard=True))
                    update.message.delete()

            else:
                if update.callback_query:
                    update.callback_query.message.edit_text('учителей нет')
                else:
                    update.message.reply_text('учителей нема') #убрать
        except APIException:
            print('сервер не работает')

    def list_keys(self, update:Update, context:CallbackContext):
        #if update.callback_query:
        #    uid = update.callback_query.message.chat.id
        #else:
        #    uid = update.message.chat.id
        try:
            list_bottuns = KEYS_API.getallKey()
            if list_bottuns:
                page = 0
                if update.callback_query:
                    try:
                        _, readpage = update.callback_query.data.split()
                        page = int(readpage)
                    except Exception:
                        pass
                object_in_page = 5
                ticher = []
                for i in list_bottuns[object_in_page*page:object_in_page*page + object_in_page]:
                    name = i['num']
                    pk = i['pk']
                    ticher.append([InlineKeyboardButton(name, callback_data=f'get_key_info {pk} {page}')])
                prevnext = []
                if page > 0:
                    prevnext.append(InlineKeyboardButton(f'{page} <<', callback_data=f'list_keys {page-1}'))
                if len(list_bottuns) > object_in_page * (page + 1):
                    prevnext.append(InlineKeyboardButton(f'>> {page+2}', callback_data=f'list_keys {page+1}'))
                if prevnext:
                    ticher.append(prevnext)
                ticher.append([InlineKeyboardButton(f'Закрыть', callback_data=f'startwq')])
                if update.callback_query:
                    update.callback_query.message.edit_text('все ключи:') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
                    update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(ticher,one_time_keyboard=False, resize_keyboard=True))
                else:
                    update.message.reply_text("все ключи:", reply_markup=InlineKeyboardMarkup(ticher,one_time_keyboard=False, resize_keyboard=True))
                    update.message.delete()
            else:
                if update.callback_query:
                    update.callback_query.message.edit_text('учителей нет')
                else:
                    update.message.reply_text('учителей нема') #убрать
        except APIException:
            print('сервер не работает')

    def user_info(self, update:Update, context:CallbackContext):
        _, user_pk, page = update.callback_query.data.split()
        user_pk = int(user_pk)
        ticher = KEYS_API.getTeacher(user_pk)
        name = ticher['name']
        partition = ticher['partition']
        take_keys = KEYS_API.getallKey()
        taking_keys = []
        for i in take_keys: 
            if name == i['teacher']:
                taking_keys.append(take_keys[i]['num'])
        keym = [
                [InlineKeyboardButton(f'Изменить инфомацию об человеке', callback_data=f'rename teacher {user_pk}')],
                [InlineKeyboardButton(f'редактировать карты', callback_data=f'card_list {user_pk}')],
                [InlineKeyboardButton(f'удалить', callback_data=f'deltich {user_pk}')],
                [InlineKeyboardButton(f'назад', callback_data=f'list_ticher {page}')]
        ]
        update.callback_query.message.edit_text(f'учитель {name}\nработает в {partition}') 
        print('1')
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(keym, one_time_keyboard=False, resize_keyboard=True))
    
    def key_info(self, update:Update, context:CallbackContext):
        _, user_pk, page = update.callback_query.data.split()
        user_pk = int(user_pk)
        keys = KEYS_API.getKey(user_pk)
        name = keys['num']
        if keys['status'] == "A":
            state = 'ключ лежит на месте'
        else:
            teacher = keys['teacher']
            state = f'ключь взял {teacher}'
        keyboard =[
                [InlineKeyboardButton(f'удалить', callback_data=f'delkey {user_pk}')],
                [InlineKeyboardButton(f'назад', callback_data=f'list_keys {page}')]
        ]
        update.callback_query.message.edit_text(f'ключ: {name}\npk ключа: {user_pk}\nсостояние: {state}') 
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(keyboard,one_time_keyboard=False, resize_keyboard=True))

    def delete_key(self, update:Update, context:CallbackContext):
        _, pk = update.callback_query.data.split()
        KEYS_API.removeKey(pk)
        key = []
        key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'list_keys 1')])
        update.callback_query.message.edit_text('ключ удален пути назад нет') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(key,one_time_keyboard=False, resize_keyboard=True))
    
    def delete_ticher(self, update:Update, context:CallbackContext):
        _, pk = update.callback_query.data.split()
        KEYS_API.removeTeacher(pk)
        key = []
        key.append([InlineKeyboardButton(f'Закрыть', callback_data=f'list_ticher 1')])
        update.callback_query.message.edit_text('учитель удален пути назад нет') #,reply_markup=InlineKeyboardMarkup(ST.page2,one_time_keyboard=False, resize_keyboard=True))
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(key,one_time_keyboard=False, resize_keyboard=True))

    def delete_message(self, update:Update, context:CallbackContext):
        update.callback_query.message.delete()

    def rmcard(self, update:Update, context:CallbackContext):
        _, pk, teacher = update.callback_query.data.split()
        KEYS_API.removeCard(pk)
        listc = list()
        listc.append([InlineKeyboardButton(f'назад', callback_data=f'card_list {teacher}')])
        update.callback_query.message.edit_text(f'удаленно')
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(listc,one_time_keyboard=False, resize_keyboar=True))

    def mycards(self, update:Update, context:CallbackContext):
        _, pk, teacher = update.callback_query.data.split()
        pipl = KEYS_API.getTeacher(teacher)
        listc = list()
        listc.append([InlineKeyboardButton(f'удалить', callback_data=f'rmcard {pk} {teacher}')])
        listc.append([InlineKeyboardButton(f'Закрыть', callback_data=f'card_list {teacher}')])
        update.callback_query.message.edit_text(f'карточка\nкод:\nпренадлежит: {pipl}')
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(listc,one_time_keyboard=False, resize_keyboard=True))

    def card_list(self, update:Update, context:CallbackContext):
        _, teachers = update.callback_query.data.split()
        a = KEYS_API.getCards()
        listc = list()
        teachers = int(teachers)
        for i in a:
            code = i['code']
            pk = i['pk']
            selteach = i['teacher']
            if teachers == selteach:
                listc.append([InlineKeyboardButton(f'карточка: {code}', callback_data=f'cardqw {pk} {teachers}')])
        listc.append([InlineKeyboardButton(f'Закрыть', callback_data=f'list_ticher 1')])
        update.callback_query.message.edit_text(f'ключи')
        update.callback_query.message.edit_reply_markup(reply_markup=InlineKeyboardMarkup(listc,one_time_keyboard=False, resize_keyboard=True))

    def run(self):    
        logging.info('Starting bot...')
        self.bot.start_polling()
        self.bot.idle()



if __name__ == "__main__":
        bot = bot()
        bot.run()