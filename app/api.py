import requests as r

class APIException(Exception):
    pass


class API:
    def __init__(self, base_url):
        self.baseURL = base_url
    
    def getTeachers(self):
        url = self.baseURL + 'teacher/'
        resp = r.get(url)
        if resp.status_code != 200:
            raise APIException('GET /teacher/ {}'.format(resp.status_code))
        return resp.json()
    
    def getTeacher(self, pk):
        url = self.baseURL + f'teacher/{pk}'
        resp = r.get(url)
        if resp.status_code != 200:
            raise APIException('GET /teacher/ {}'.format(resp.status_code))
        return resp.json()
    
    def getCards(self):
        url = self.baseURL + 'card/'
        resp = r.get(url)
        if resp.status_code != 200:
            raise APIException('GET /card/ {}'.format(resp.status_code))
        return resp.json()
    
    def getKey(self, key):
        url = self.baseURL + f'key/{key}/'
        resp = r.get(url)
        if resp.status_code != 200:
            raise APIException(f'GET /key/{key}/ {resp.status_code}')
        return resp.json()

    def getallKey(self):
        url = self.baseURL + 'key/'
        resp = r.get(url)
        if resp.status_code != 200:
            raise APIException(f'GET /key/ {resp.status_code}')
        return resp.json()
    
    def takeKey(self, key, teacher):
        url = self.baseURL + f'key/{key}/'
        data = {'status': 'T', 'teacher': teacher}
        resp = r.patch(url, json=data)
        if resp.status_code != 200:
            raise APIException(f'PATCH /key/{key}/ {resp.status_code}')
        url = self.baseURL + f'key_history/'
        data = {'key': key, 'teacher': teacher, 'event_type': 'T'}
        resp = r.post(url, json=data)
        if resp.status_code != 201:
            raise APIException(f'POST /key_history/ {resp.status_code}')
    
    def leaveKey(self, key, teacher):
        url = self.baseURL + f'key/{key}/'
        data = {'status': 'A', 'teacher': None}
        resp = r.patch(url, json=data)
        if resp.status_code != 200:
            raise APIException(f'PATCH /key/{key}/ {resp.status_code}')
        url = self.baseURL + f'key_history/'
        data = {'key': key, 'teacher': teacher, 'event_type': 'L'}
        resp = r.post(url, json=data)
        if resp.status_code != 201:
            raise APIException(f'POST /key_history/ {resp.status_code}')
    
    def getTeacherFromCard(self, code):
        cards = self.getCards()
        for card in cards:
            if card['code'] == code:
                return card['teacher']
        return None
    
    def addTeacher(self, name, partition):
        url = self.baseURL + f'teacher/'
        data = {'name': name, 'partition': partition}
        resp = r.post(url, json=data)
        if resp.status_code != 201:
            raise APIException(f'POST /teacher/ {resp.status_code}')
    
    def removeTeacher(self, teacher):
        url = self.baseURL + f'teacher/{teacher}/'
        resp = r.delete(url)
        if resp.status_code != 204:
            raise APIException(f'DELETE /teacher/ {resp.status_code}')
    
    def changeTeacher(self, teacher, name, partition):
        url = self.baseURL + f'teacher/{teacher}/'
        data = {'name': name, 'partition': partition}
        resp = r.patch(url, json=data)
        if resp.status_code != 204:
            raise APIException(f'DELETE /teacher/ {resp.status_code}')
    
    def addKey(self, num):
        url = self.baseURL + f'key/'
        data = {'num': num}
        resp = r.post(url, json=data)
        if resp.status_code != 201:
            raise APIException(f'POST /key/ {resp.status_code}')
    
    def removeKey(self, key):
        url = self.baseURL + f'key/{key}/'
        resp = r.delete(url)
        if resp.status_code != 204:
            raise APIException(f'DELETE /key/ {resp.status_code}')

    def addCard(self, code, teacher):
        url = self.baseURL + f'card/'
        data = {'code': code, 'teacher': teacher}
        resp = r.post(url, json=data)
        if resp.status_code != 201:
            raise APIException(f'POST /card/ {resp.status_code}')
    
    
    def removeCard(self, card):
        url = self.baseURL + f'card/{card}/'
        resp = r.delete(url)
        if resp.status_code != 204:
            raise APIException(f'DELETE /card/ {resp.status_code}')
        


KEYS_API = API("https://www.keys.ibl.pw/api/")