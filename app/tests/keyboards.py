from telegram import InlineKeyboardButton

from services.keyboards import ButtonContainer, KeyboardBuilder, Button

def test_keyboard_builder():
    keyboard = KeyboardBuilder(
        ButtonContainer(
            Button("test", "callback")
            .add("test3", "callback3")
            .to_list(),
            columns=2,
        )
        .add(
            Button("test2", "callback2")
            .to_list(),
        )
        .to_list()
    )()
    success_keyboard = [
        [
            InlineKeyboardButton(
                "test",
                callback_data="callback",
            ),
            InlineKeyboardButton(
                "test3",
                callback_data="callback3",
            )
        ],
        [
            InlineKeyboardButton(
                "test2",
                callback_data="callback2",
            )
        ],
    ]

    assert keyboard == success_keyboard
