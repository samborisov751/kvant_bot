from telegram import InlineKeyboardMarkup, InlineKeyboardButton


class KeyboardBuilder:
    """Constructs keyboards"""

    def __init__(self, keys_data: list):
        """Initiates keyboard params"""
        self._keys_data = keys_data

    def compile_keyboard_buttons(self):
        """Compiles keyboard buttons data"""
        keyboard_data = []
        for container in self._keys_data:
            container: ButtonContainer
            data: list = container.data
            while len(data) > 0:
                row: list = data[:container.columns]
                data = data[container.columns:]
                keyboard_data.append(
                    list(map(
                        lambda key: InlineKeyboardButton(
                            key.title,
                            callback_data=key.callback,
                        ),
                        row,
                    ))
                )
        return keyboard_data

    def __call__(self, *args, **kwargs):
        """Returns compiled keyboard buttons data"""
        return self.compile_keyboard_buttons()


class Button:
    """Contains keyboard buttons data and parameters"""

    def __init__(self, title: str, callback: str):
        """Initiates container"""
        self._stack = []
        self._title = title
        self._callback = callback

    @property
    def title(self):
        return self._title

    @property
    def callback(self):
        return self._callback

    def add(self, *args, **kwargs):
        """Adds new container into stack"""
        self._stack.append(Button(*args, **kwargs))
        return self

    def to_list(self):
        # TODO: change to_list to actual list behavior
        """Represents container as list"""
        return [self] + self._stack


class ButtonContainer:
    """Contains keyboard buttons data and parameters"""

    def __init__(self, keys_data: list, columns: int = 1):
        """Initiates container"""
        self._stack = []
        self._keys_data = keys_data
        self._columns = columns

    @property
    def data(self):
        return self._keys_data

    @property
    def columns(self):
        return self._columns

    def add(self, *args, **kwargs):
        """Adds new container into stack"""
        self._stack.append(ButtonContainer(*args, **kwargs))
        return self

    def to_list(self):
        # TODO: change to_list to actual list behavior
        """Represents container as list"""
        return [self] + self._stack


class KeyboardMarkupProvider:
    """Provides keyboards used in bot"""

    def __init__(self):
        """Just init"""
        pass

    @property
    def main_keyboard_markup(self):
        """Main keyboard used to start user navigation"""
        buttons_data = [
            ButtonContainer(
                Button("Ключи", "ключи")
                .add("Учителя", "учителя")
                .to_list()
            )
            .to_list()
        ]
        return InlineKeyboardMarkup(
            KeyboardBuilder(buttons_data)(),
            one_time_keyboard=False,
            resize_keyboard=True,
        )

    def paged_keyboard(self, data: list):
        button = Button(data[0]["name"], "get_user_info " + data[0]["pk"])
        for item in data[1:]:
            button.add(item["name"], "get_user_info " + item["pk"])
        return InlineKeyboardMarkup(
            KeyboardBuilder(
                ButtonContainer(button.to_list())
                .to_list()
            )
        )