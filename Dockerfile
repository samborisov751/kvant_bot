FROM python:3.9-slim-buster
RUN mkdir /app
WORKDIR /app
RUN pip install pipenv
COPY Pipfile /app
COPY Pipfile.lock /app
RUN pipenv install --deploy --system --ignore-pipfile
COPY app/. /app
ENTRYPOINT [ "python", "bot.py" ]
